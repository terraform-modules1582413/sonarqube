variable "tolerations_mapping" {
  default = {
    "NO_SCHEDULE"        = "NoSchedule",
    "PREFER_NO_SCHEDULE" = "PreferNoSchedule",
    "NO_EXECUTE"         = "NoExecute"
  }
}

locals {
  ingress_domain = "${module.name.pretty}.${var.cf_base_domain}"
  tolerations = [
    for taint in var.sonarqube_taints : {
      key      = taint.key
      value    = taint.value
      effect   = lookup(var.tolerations_mapping, taint.effect)
      operator = "Equal"
    }
  ]
}

resource "kubernetes_secret" "repository" {
  metadata {
    name      = "sonarqube-repo"
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }
  data = {
    project = var.argocd_project
    type    = "git"
    url     = var.sonarqube_repo_url
  }
  type = "Opaque"
}

resource "time_sleep" "wait_10_seconds" {
  depends_on = [kubernetes_secret.repository]

  create_duration = "10s"
}

resource "kubernetes_manifest" "argocd_application" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-application.yaml", {
      argocd_name      = "sonarqube"
      argocd_namespace = var.argocd_namespace
      argocd_project   = var.argocd_project
      environment      = var.project_environment
      tolerations      = local.tolerations
      repo             = var.sonarqube_repo_url
      namespace        = var.sonarqube_namespace
      crt_name         = var.sonarqube_crt_name
      crt              = var.sonarqube_crt
      crt_key          = var.sonarqube_crt_key
      domain           = local.ingress_domain
      ingress_class    = var.ingress_class
    })
  )
  depends_on = [kubernetes_secret.repository, time_sleep.wait_10_seconds]
}

resource "cloudflare_record" "sonarqube" {
  zone_id = data.cloudflare_zone.main.zone_id
  name    = module.name.pretty
  value   = var.ingress_public_ip
  type    = "A"
  ttl     = "300"
  proxied = false
}
